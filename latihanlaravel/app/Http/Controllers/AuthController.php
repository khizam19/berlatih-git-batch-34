<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg()
    {
        return view('page.register'); 
    }

    public function welcome(request $request)
    {
        // dd($request->all());
        $depan = $request['fname'];
        $belakang = $request['lname'];
        $gender = $request['Female'];
        $negara = $request['negara'];
        $cekbox = $request['indonesia'];
        $biodata = $request['bio'];


        return view('page.welcome', compact('depan', 'belakang'));
        
    }
}
